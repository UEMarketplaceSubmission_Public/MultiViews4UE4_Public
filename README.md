# Relative Document:

**MultiWindows** Example Projects And Documents: **DOC for MultiWindows Setting**

[https://gitlab.com/UE4MarketplaceSubmission_Public/multiwindows4ue4_public](https://redirect.epicgames.com/?redirectTo=https://gitlab.com/UE4MarketplaceSubmission_Public/multiwindows4ue4_public)

# MultiViews4UE4

We setup 4 views in this Demo Project: "MultiViewsUE4Demo".
![](README.md.res/01_Images/0.png)

## Create And Enable MultiViews For your UE4 Project

![](README.md.res/01_Images/1.png)

![](README.md.res/01_Images/3.png)

## How to setup multi-views

### The First Way: Set "ViewManager" for "MultiViewLocalPlayer" directly

![](README.md.res/01_Images/2.png)

### The second way: Use "WBP_View" UMG Widget to setup views

![](README.md.res/01_Images/5.png)

## Modify the settings of one View
![](README.md.res/01_Images/4.png)


## Properties of One Viewpoint

![](README.md.res/01_Images/10.png)

### Viewpoint Type

![](README.md.res/01_Images/6.png)

#### 1. ViewpointType: "CustomViewPoint"
![](README.md.res/01_Images/9.png)

#### 2. ViewpointType: "BindToPlayerController"
![](README.md.res/01_Images/8.png)

#### 3. ViewpointType: "BindToViewTarget"

![](README.md.res/01_Images/7.png)

## How to fix the issues of Multi Views Splicing

### 1. Set the Screen Resolution  of Windows System to 100%, not 150% or others

### 2. Close some Post-Process effects

- Close the Vignette(Set the value of Vignette to 0 in the PostProcessVolume or Camera PostProcess)
- Close Screen-Space AO
- Close Screen-Space Reflection
- Close Motion Blur: In the ProjectSettings->Rendering
- Close **Auto Exposure**: In the ProjectSettings->Rendering

### 3. Use absolute position for setting the location of Views，to avoid pixel lost after "float" converted to "int".



## Quick use guide

### How to use "Bind to ViewTarget" ViewpointType in the "ViewManager"?

**Can also view the same document here:**

**《1.20 How to use "Bind to ViewTarget" ViewpointType in the "ViewManager"》**

[https://gitlab.com/UEMarketplaceSubmission_Public/multiwindows4ue4_public#120-how-to-use-bind-to-viewtarget-viewpointtype-in-the-viewmanager](https://redirect.epicgames.com/?redirectTo=https://gitlab.com/UEMarketplaceSubmission_Public/multiwindows4ue4_public#120-how-to-use-bind-to-viewtarget-viewpointtype-in-the-viewmanager)

#### Example 01: 

##### Issue Description:

![image-20231031221556350](README/00_Res/01_Images/image-20231031221556350.png)

##### Code Example:

###### Winodw Manager Actor: "BPA_MultiWindows"

![image-20231031222938480](README/00_Res/01_Images/image-20231031222938480.png)

###### ViewTargetActor is a "BPA_Camera" Actor which is attached to the Character's Camera Component:

![image-20231031223017540](README/00_Res/01_Images/image-20231031223017540.png)

![image-20231031223234662](README/00_Res/01_Images/image-20231031223234662.png)

###### Configure the "ViewTarget" of "ViewManaer" for the "BPA_MultiWindows" Actor in Scene "Outliner":

![image-20231031223424505](README/00_Res/01_Images/image-20231031223424505.png)

###### Result: Left Window is First-Person Perspective; Right Window is Third-Person Perspective.

![image-20231031223634005](README/00_Res/01_Images/image-20231031223634005.png)
